package br.ucsal.bes20191.bd2.exerciciojpa2.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tab_ramo_atividade")
@SequenceGenerator(name = "sq_ramo_atividade", sequenceName = "sq_ramo_atividade")
public class RamoAtividade {

	@Id
	@GeneratedValue(generator = "sq_ramo_atividade")
	private Integer id;

	@Column(length = 40, nullable = false)
	private String nome;

	public RamoAtividade() {
		super();
	}

	public RamoAtividade(String nome) {
		super();
		this.nome = nome;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "RamoAtividade [id=" + id + ", nome=" + nome + "]";
	}

}
