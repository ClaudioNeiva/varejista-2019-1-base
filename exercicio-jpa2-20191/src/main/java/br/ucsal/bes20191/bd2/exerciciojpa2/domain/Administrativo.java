package br.ucsal.bes20191.bd2.exerciciojpa2.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tab_administrativo")
public class Administrativo extends Funcionario {

	private Integer turno;// - integer - not null

	public Administrativo() {
		super();
	}

	public Administrativo(String cpf, String nome, String rg, String rgOrgaoExpedidor, String rgUf,
			List<String> telefones, Date dataNascimento, Endereco endereco, Integer turno) {
		super(cpf, nome, rg, rgOrgaoExpedidor, rgUf, telefones, dataNascimento, endereco);
		this.turno = turno;
	}

	public Integer getTurno() {
		return turno;
	}

	public void setTurno(Integer turno) {
		this.turno = turno;
	}

	@Override
	public String toString() {
		return "Administrativo [turno=" + turno + ", toString()=" + super.toString() + "]";
	}

}
