package br.ucsal.bes20191.bd2.exerciciojpa2.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "tab_funcionario", uniqueConstraints = {
		@UniqueConstraint(name = "un_funcionario_rg", columnNames = { "rg", "rg_orgao_expedidor", "rg_uf" }) })
@Inheritance(strategy = InheritanceType.JOINED)
public class Funcionario {

	@Id
	@Column(columnDefinition = "char(11)", nullable = false)
	private String cpf;

	@Column(length = 40, nullable = false)
	private String nome;

	@Column(columnDefinition = "char(12)", nullable = true)
	private String rg;

	@Column(length = 20, nullable = true, name = "rg_orgao_expedidor")
	private String rgOrgaoExpedidor;

	@Column(columnDefinition = "char(2)", nullable = true, name = "rg_uf")
	private String rgUf;

	@ElementCollection
	@Column(name = "telefone", length = 12)
	@CollectionTable(name = "tab_funcionario_telefone", joinColumns = @JoinColumn(name = "cpf"))
	private List<String> telefones;

	@Temporal(TemporalType.DATE)
	@Column(nullable = false, name = "data_nascimento")
	private Date dataNascimento;

	@Embedded
	@Column
	private Endereco endereco;

	public Funcionario() {
		super();
	}

	public Funcionario(String cpf, String nome, String rg, String rgOrgaoExpedidor, String rgUf, List<String> telefones,
			Date dataNascimento, Endereco endereco) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.rg = rg;
		this.rgOrgaoExpedidor = rgOrgaoExpedidor;
		this.rgUf = rgUf;
		this.telefones = telefones;
		this.dataNascimento = dataNascimento;
		this.endereco = endereco;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getRgOrgaoExpedidor() {
		return rgOrgaoExpedidor;
	}

	public void setRgOrgaoExpedidor(String rgOrgaoExpedidor) {
		this.rgOrgaoExpedidor = rgOrgaoExpedidor;
	}

	public String getRgUf() {
		return rgUf;
	}

	public void setRgUf(String rgUf) {
		this.rgUf = rgUf;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	@Override
	public String toString() {
		return "Funcionario [cpf=" + cpf + ", nome=" + nome + ", rg=" + rg + ", rgOrgaoExpedidor=" + rgOrgaoExpedidor
				+ ", rgUf=" + rgUf + ", telefones=" + telefones + ", dataNascimento=" + dataNascimento + ", endereco="
				+ endereco + "]";
	}

}
