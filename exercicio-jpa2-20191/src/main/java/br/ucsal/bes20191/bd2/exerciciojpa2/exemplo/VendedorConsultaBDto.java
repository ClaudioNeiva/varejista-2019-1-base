package br.ucsal.bes20191.bd2.exerciciojpa2.exemplo;

import java.util.Collection;
import java.util.List;

import br.ucsal.bes20191.bd2.exerciciojpa2.domain.Endereco;

public class VendedorConsultaBDto {
	
	private String nome;
	private Collection<String> telefones;
	private Endereco endereco;

	public VendedorConsultaBDto(String nome, Endereco endereco) {
		super();
		this.nome = nome;
		this.telefones = telefones;
		this.endereco = endereco;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Collection<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(Collection<String> telefones) {
		this.telefones = telefones;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

}
