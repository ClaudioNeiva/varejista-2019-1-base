package br.ucsal.bes20191.bd2.exerciciojpa2.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import br.ucsal.bes20191.bd2.exerciciojpa2.converter.SituacaoVendedorConverter;

@Entity
@Table(name = "tab_vendedor")
public class Vendedor extends Funcionario {

	@Column(precision = 10, scale = 2, nullable = false, name = "percentual_comissao")
	private BigDecimal percentualComissao;

	@Column(nullable = false, columnDefinition = "char(3)")
	@Convert(converter = SituacaoVendedorConverter.class)
	private SituacaoVendedorEnum situacao;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "tab_vendedor_cliente", joinColumns = @JoinColumn(name = "cpf_vendedor"), inverseJoinColumns = @JoinColumn(name = "cnpj_cliente"))
	private List<PessoaJuridica> clientes;

	public Vendedor() {
		super();
	}

	public Vendedor(String cpf, String nome, String rg, String rgOrgaoExpedidor, String rgUf, List<String> telefones,
			Date dataNascimento, Endereco endereco, BigDecimal percentualComissao, SituacaoVendedorEnum situacao, List<PessoaJuridica> clientes) {
		super(cpf, nome, rg, rgOrgaoExpedidor, rgUf, telefones, dataNascimento, endereco);
		this.percentualComissao = percentualComissao;
		this.situacao = situacao;
		this.clientes = clientes;
	}

	public BigDecimal getPercentualComissao() {
		return percentualComissao;
	}

	public void setPercentualComissao(BigDecimal percentualComissao) {
		this.percentualComissao = percentualComissao;
	}

	public SituacaoVendedorEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoVendedorEnum situacao) {
		this.situacao = situacao;
	}

	public List<PessoaJuridica> getClientes() {
		return clientes;
	}

	public void setClientes(List<PessoaJuridica> clientes) {
		this.clientes = clientes;
	}

	@Override
	public String toString() {
		return "Vendedor [percentualComissao=" + percentualComissao + ", situacao=" + situacao + ", clientes="
				+ clientes + ", toString()=" + super.toString() + "]";
	}

}
