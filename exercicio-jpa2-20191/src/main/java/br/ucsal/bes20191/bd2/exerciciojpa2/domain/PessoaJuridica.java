package br.ucsal.bes20191.bd2.exerciciojpa2.domain;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tab_pessoa_juridica")
public class PessoaJuridica {

	@Id
	@Column(columnDefinition = "char(13)", nullable = false)
	private String cnpj;

	@Column(length = 40, nullable = false)
	private String nome;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "tab_pessoa_juridica_ramo_atividade", joinColumns = @JoinColumn(name = "cnpj_pessoa_juridica"), inverseJoinColumns = @JoinColumn(name = "id_ramo_atividade"))
	private List<RamoAtividade> ramosAtividade;

	@Column(precision = 10, scale = 2)
	private BigDecimal faturamento;

	@ManyToMany(cascade = CascadeType.ALL, mappedBy="clientes")
	private List<Vendedor> vendedores;

	public PessoaJuridica() {
		super();
	}

	public PessoaJuridica(String cnpj, String nome, List<RamoAtividade> ramosAtividade, BigDecimal faturamento) {
		super();
		this.cnpj = cnpj;
		this.nome = nome;
		this.ramosAtividade = ramosAtividade;
		this.faturamento = faturamento;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<RamoAtividade> getRamosAtividade() {
		return ramosAtividade;
	}

	public void setRamosAtividade(List<RamoAtividade> ramosAtividade) {
		this.ramosAtividade = ramosAtividade;
	}

	public BigDecimal getFaturamento() {
		return faturamento;
	}

	public void setFaturamento(BigDecimal faturamento) {
		this.faturamento = faturamento;
	}

	public List<Vendedor> getVendedores() {
		return vendedores;
	}

	public void setVendedores(List<Vendedor> vendedores) {
		this.vendedores = vendedores;
	}

	@Override
	public String toString() {
		return "PessoaJuridica [cnpj=" + cnpj + ", nome=" + nome + ", ramosAtividade=" + ramosAtividade
				+ ", faturamento=" + faturamento + ", vendedores=" + vendedores + "]";
	}

}
