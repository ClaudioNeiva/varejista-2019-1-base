package br.ucsal.bes20191.bd2.exerciciojpa2.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tab_cidade")
public class Cidade {

	@Id
	@Column(columnDefinition = "char(3)", nullable = false)
	private String sigla;

	@Column(length = 40, nullable = false)
	private String nome;

	@ManyToOne(optional = false, cascade = CascadeType.ALL)
	private Estado estado;

	public Cidade() {
		super();
	}

	public Cidade(String sigla, String nome, Estado estado) {
		super();
		this.sigla = sigla;
		this.nome = nome;
		this.estado = estado;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "Cidade [sigla=" + sigla + ", nome=" + nome + ", estado=" + estado + "]\n";
	}

}
