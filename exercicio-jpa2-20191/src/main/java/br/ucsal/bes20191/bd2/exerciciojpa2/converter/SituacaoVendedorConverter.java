package br.ucsal.bes20191.bd2.exerciciojpa2.converter;

import javax.persistence.AttributeConverter;

import br.ucsal.bes20191.bd2.exerciciojpa2.domain.SituacaoVendedorEnum;

public class SituacaoVendedorConverter implements AttributeConverter<SituacaoVendedorEnum, String> {

	@Override
	public String convertToDatabaseColumn(SituacaoVendedorEnum attribute) {
		return attribute != null ? attribute.getCodigo() : null;
	}

	@Override
	public SituacaoVendedorEnum convertToEntityAttribute(String dbData) {
		return dbData != null ? SituacaoVendedorEnum.valorOfCodigo(dbData) : null;
	}

}
