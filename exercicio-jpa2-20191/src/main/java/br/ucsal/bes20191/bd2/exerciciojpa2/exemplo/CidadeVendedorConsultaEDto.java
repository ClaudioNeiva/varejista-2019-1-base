package br.ucsal.bes20191.bd2.exerciciojpa2.exemplo;

public class CidadeVendedorConsultaEDto {

	private String nomeCidade;

	private long qtdVendedores;

	public CidadeVendedorConsultaEDto(String nomeCidade, long qtdVendedores) {
		super();
		this.nomeCidade = nomeCidade;
		this.qtdVendedores = qtdVendedores;
	}

	public String getNomeCidade() {
		return nomeCidade;
	}

	public void setNomeCidade(String nomeCidade) {
		this.nomeCidade = nomeCidade;
	}

	public long getQtdVendedores() {
		return qtdVendedores;
	}

	public void setQtdVendedores(long qtdVendedores) {
		this.qtdVendedores = qtdVendedores;
	}

	@Override
	public String toString() {
		return "CidadeVendedorConsultaEDto [nomeCidade=" + nomeCidade + ", qtdVendedores=" + qtdVendedores + "]\n";
	}

}
