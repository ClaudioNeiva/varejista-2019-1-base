package br.ucsal.bes20191.bd2.exerciciojpa2.exemplo;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import javax.persistence.EntityManager;

import br.ucsal.bes20191.bd2.exerciciojpa2.domain.Administrativo;
import br.ucsal.bes20191.bd2.exerciciojpa2.domain.Cidade;
import br.ucsal.bes20191.bd2.exerciciojpa2.domain.Endereco;
import br.ucsal.bes20191.bd2.exerciciojpa2.domain.Estado;
import br.ucsal.bes20191.bd2.exerciciojpa2.domain.PessoaJuridica;
import br.ucsal.bes20191.bd2.exerciciojpa2.domain.RamoAtividade;
import br.ucsal.bes20191.bd2.exerciciojpa2.domain.SituacaoVendedorEnum;
import br.ucsal.bes20191.bd2.exerciciojpa2.domain.Vendedor;

public class Carga {

	private static DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

	public static void carregarDados(EntityManager em) throws ParseException {
		// 3 estados;
		Estado estadoBA = new Estado("BA", "Bahia");
		Estado estadoSP = new Estado("SP", "S�o Paulo");
		Estado estadoRJ = new Estado("RJ", "Rio de Janeiro");

		// 5 cidades em 2 estados distintos (1 dos estados n�o deve ter cidade);
		Cidade cidadeSSA = new Cidade("SSA", "Salvador", estadoBA);
		Cidade cidadeFDS = new Cidade("FDS", "Feira de Santana", estadoBA);
		Cidade cidadeALG = new Cidade("ALG", "Alagoinhas", estadoBA);
		Cidade cidadeCPJ = new Cidade("CPJ", "Campos do Jord�o", estadoSP);
		Cidade cidadeCMP = new Cidade("CMP", "Campinas", estadoSP);

		// 3 ramos de atividade;
		// Aten��o!!!! Problema AQUI!!!!
		RamoAtividade ramoAtividadeCalcado = new RamoAtividade("Cal�ado");
		RamoAtividade ramoAtividadeVestuario = new RamoAtividade("Vestu�rio");
		RamoAtividade ramoAtividadeArmas = new RamoAtividade("Armas");

		// 5 pessoas jur�dicas em 2 ramos de atividade (1 ramo de atividade n�o
		// deve ter pessoa jur�dica associada);
		PessoaJuridica pessoaJuridica1 = new PessoaJuridica("123", "Mesbla",
				Arrays.asList(ramoAtividadeCalcado, ramoAtividadeVestuario), BigDecimal.valueOf(100000));

		PessoaJuridica pessoaJuridica2 = new PessoaJuridica("234", "Sandiz", Arrays.asList(ramoAtividadeVestuario),
				BigDecimal.valueOf(9000000));

		PessoaJuridica pessoaJuridica3 = new PessoaJuridica("345", "Le�o de Ouro", Arrays.asList(ramoAtividadeCalcado),
				BigDecimal.valueOf(100000));

		PessoaJuridica pessoaJuridica4 = new PessoaJuridica("456", "Renner", Arrays.asList(ramoAtividadeVestuario),
				BigDecimal.valueOf(9998888));

		PessoaJuridica pessoaJuridica5 = new PessoaJuridica("576", "Riachuelo", Arrays.asList(ramoAtividadeVestuario),
				BigDecimal.valueOf(88889999));

		// 5 vendedores (mais de um cliente associado ao mesmo vendedor e mais
		// de um vendedor associado ao mesmo cliente, podem existir clientes sem
		// vendedor e vendedor sem cliente);
		Vendedor vendedor1 = new Vendedor("312", "Claudio", null, null, null, Arrays.asList("12312", "7688"),
				df.parse("03/05/1900"), new Endereco("Rua A", "Caju", cidadeSSA), BigDecimal.valueOf(1),
				SituacaoVendedorEnum.ATIVO, Arrays.asList(pessoaJuridica1));
		Vendedor vendedor2 = new Vendedor("432", "Antonio", null, null, null,
				Arrays.asList("788787", "4576456", "345634"), df.parse("08/01/1990"),
				new Endereco("Rua B", "Jaca", cidadeSSA), BigDecimal.valueOf(1), SituacaoVendedorEnum.ATIVO,
				Arrays.asList(pessoaJuridica1, pessoaJuridica2));
		Vendedor vendedor3 = new Vendedor("543", "Pedro", null, null, null, Arrays.asList("689978"),
				df.parse("13/08/1981"), new Endereco("Av C", "Manga", cidadeCMP), BigDecimal.valueOf(1),
				SituacaoVendedorEnum.ATIVO, Arrays.asList(pessoaJuridica2));
		Vendedor vendedor4 = new Vendedor("654", "Ana", null, null, null, Arrays.asList("12123"),
				df.parse("23/09/1910"), new Endereco("Av D", "Melancia", cidadeFDS), BigDecimal.valueOf(1),
				SituacaoVendedorEnum.SUSPENSO, null);
		Vendedor vendedor5 = new Vendedor("765", "Clara", null, null, null, Arrays.asList("7647"),
				df.parse("11/12/1989"), new Endereco("Rua X", "Goiaba", cidadeSSA), BigDecimal.valueOf(1),
				SituacaoVendedorEnum.ATIVO, Arrays.asList(pessoaJuridica1, pessoaJuridica4));

		// 3 funcion�rios administrativos;
		Administrativo administrativo1 = new Administrativo("111", "Joana", "12312", "SSP", "BA", Arrays.asList("645"),
				df.parse("05/09/2000"), new Endereco("Rua Z", "Jaca", cidadeSSA), 1);
		Administrativo administrativo2 = new Administrativo("222", "Maria", null, null, null, Arrays.asList("567567"),
				df.parse("07/03/2005"), new Endereco("Rua Z", "Jaca", cidadeSSA), 2);
		Administrativo administrativo3 = new Administrativo("333", "Joaquim", null, null, null,
				Arrays.asList("3563454"), df.parse("08/01/2001"), new Endereco("Rua X", "Caju", cidadeFDS), 1);

		// Persist�ncia dos objetos

		em.getTransaction().begin();
		em.persist(estadoBA);
		em.persist(estadoRJ);
		em.persist(estadoSP);
		em.persist(cidadeALG);
		em.persist(cidadeCMP);
		em.persist(cidadeCPJ);
		em.persist(cidadeFDS);
		em.persist(cidadeSSA);
		em.persist(ramoAtividadeArmas);
		em.persist(ramoAtividadeCalcado);
		em.persist(ramoAtividadeVestuario);
		em.persist(pessoaJuridica1);
		em.persist(pessoaJuridica2);
		em.persist(pessoaJuridica3);
		em.persist(pessoaJuridica4);
		em.persist(pessoaJuridica5);
		em.persist(vendedor1);
		em.persist(vendedor2);
		em.persist(vendedor3);
		em.persist(vendedor4);
		em.persist(vendedor5);
		em.getTransaction().commit();

	}

}
