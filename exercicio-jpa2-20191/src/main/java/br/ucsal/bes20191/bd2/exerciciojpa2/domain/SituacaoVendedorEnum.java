package br.ucsal.bes20191.bd2.exerciciojpa2.domain;

public enum SituacaoVendedorEnum {

	ATIVO("ATV"), SUSPENSO("SPN");

	private String codigo;

	private SituacaoVendedorEnum(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public static SituacaoVendedorEnum valorOfCodigo(String codigo) {
		for (SituacaoVendedorEnum situacaoVendedorEnum : values()) {
			if (situacaoVendedorEnum.codigo.equalsIgnoreCase(codigo)) {
				return situacaoVendedorEnum;
			}
		}
		throw new IllegalArgumentException("Codigo não encontrado=" + codigo);
	}

}
