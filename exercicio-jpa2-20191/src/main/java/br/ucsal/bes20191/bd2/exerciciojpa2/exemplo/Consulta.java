package br.ucsal.bes20191.bd2.exerciciojpa2.exemplo;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;

import br.ucsal.bes20191.bd2.exerciciojpa2.domain.Estado;
import br.ucsal.bes20191.bd2.exerciciojpa2.domain.PessoaJuridica;
import br.ucsal.bes20191.bd2.exerciciojpa2.domain.SituacaoVendedorEnum;
import br.ucsal.bes20191.bd2.exerciciojpa2.domain.Vendedor;

public class Consulta {

	public static void main(String[] args) {

		EntityManagerFactory emf = null;
		try {
			emf = Persistence.createEntityManagerFactory("varejista");
			EntityManager em = emf.createEntityManager();

			Carga.carregarDados(em);
			excutarConsultaA(em);
			excutarConsultaA2(em);
			excutarConsultaB(em);
			excutarConsultaB2(em);
			excutarConsultaC(em);
			excutarConsultaD(em);
			excutarConsultaD2(em);
			excutarConsultaE(em);
			excutarConsultaF(em);

			em.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

	}

	private static void excutarConsultaA(EntityManager em) {
		// Clientes por vendedor
		// Filtro: nome vendedor
		// Resultado: nome e cnpj dos clientes desse vendedor
		System.out.println("********************* excutarConsultaA *******************************");
		String jpql = "select v from Vendedor v where v.nome = :nomeVendedor";
		TypedQuery<Vendedor> query = em.createQuery(jpql, Vendedor.class);
		query.setParameter("nomeVendedor", "Claudio");
		Vendedor vendedor = query.getSingleResult();
		for (PessoaJuridica cliente : vendedor.getClientes()) {
			System.out.println(cliente.getNome() + "|" + cliente.getCnpj());
		}
	}

	private static void excutarConsultaA2(EntityManager em) {
		// Clientes por vendedor
		// Filtro: nome vendedor
		// Resultado: nome e cnpj dos clientes desse vendedor
		System.out.println("******************** excutarConsultaA2 ********************************");
		String jpql = "select c from Vendedor v inner join v.clientes c where v.nome = :nomeVendedor";
		TypedQuery<PessoaJuridica> query = em.createQuery(jpql, PessoaJuridica.class);
		query.setParameter("nomeVendedor", "Antonio");
		List<PessoaJuridica> clientes = query.getResultList();
		for (PessoaJuridica cliente : clientes) {
			System.out.println(cliente.getNome() + "|" + cliente.getCnpj());
		}
	}

	private static void excutarConsultaB(EntityManager em) {
		// Ramos de atividade
		// Filtro: nome do ramo atividade
		// Resultado: nome, telefone e endere�o dos vendedores que atendem �
		// clientes desse ramo de atividade
		System.out.println("************************ excutarConsultaB ****************************");
		String jpql = "select v from Vendedor v inner join v.clientes c inner join c.ramosAtividade ra where ra.nome = :nomeRamoAtividade";
		TypedQuery<Vendedor> query = em.createQuery(jpql, Vendedor.class);
		query.setParameter("nomeRamoAtividade", "Vestu�rio");
		List<Vendedor> vendedores = query.getResultList();
		for (Vendedor vendedor : vendedores) {
			System.out.println(vendedor.getNome() + " | " + vendedor.getTelefones() + " | " + vendedor.getEndereco());
		}
	}

	private static void excutarConsultaB2(EntityManager em) {
		// Ramos de atividade
		// Filtro: nome do ramo atividade
		// Resultado: nome, telefone e endere�o dos vendedores que atendem �
		// clientes desse ramo de atividade
		System.out.println("************************ excutarConsultaB2 ****************************");
		String jpql = "select new br.ucsal.bes20191.bd2.exerciciojpa2.exemplo.VendedorConsultaBDto(v.nome, v.endereco) from Vendedor v inner join v.clientes c inner join c.ramosAtividade ra where ra.nome = :nomeRamoAtividade";
		TypedQuery<VendedorConsultaBDto> query = em.createQuery(jpql, VendedorConsultaBDto.class);
		query.setParameter("nomeRamoAtividade", "Vestu�rio");
		List<VendedorConsultaBDto> vendedoresDto = query.getResultList();
		for (VendedorConsultaBDto vendedorDto : vendedoresDto) {
			System.out.println(
					vendedorDto.getNome() + " | " + vendedorDto.getTelefones() + " | " + vendedorDto.getEndereco());
		}
	}

	private static void excutarConsultaC(EntityManager em) {
		// Funcionarios ativos
		// Filtro: (nenhum)
		// Resultado: cpf e nome dos funcion�rios que t�m situa��o ATIVO
		System.out.println("************************ excutarConsultaC ****************************");

		// Voc� pode indicar o item da enumera��o, desde que forne�a o qualified name,
		// mas como o mesmo fica na string, refatora��es n�o o afetam, levando �
		// poss�vel problemas.
		// String jpql = "select new
		// br.ucsal.bes20191.bd2.exerciciojpa2.exemplo.VendedorConsultaCDto(v.cpf,
		// v.nome) from Vendedor v where situacao =
		// br.ucsal.bes20191.bd2.exerciciojpa2.domain.SituacaoVendedorEnum.ATIVO" ;
		String jpql = "select new " + VendedorConsultaCDto.class.getCanonicalName()
				+ "(v.cpf, v.nome) from Vendedor v where situacao = :situacao";
		TypedQuery<VendedorConsultaCDto> query = em.createQuery(jpql, VendedorConsultaCDto.class);
		query.setParameter("situacao", SituacaoVendedorEnum.ATIVO);
		List<VendedorConsultaCDto> vendedoresDto = query.getResultList();
		System.out.println(vendedoresDto);
	}

	private static void excutarConsultaD(EntityManager em) {
		// Estados sem cidade
		// Filtro: (nenhum)
		// Resultado: sigla e nome dos estados que n�o possuem cidades
		// cadastradas
		System.out.println("************************ excutarConsultaD ****************************");
		String jpql = "select e from Estado e where e not in (select c.estado from Cidade c)";
		TypedQuery<Estado> query = em.createQuery(jpql, Estado.class);
		List<Estado> estados = query.getResultList();
		System.out.println(estados);
	}

	private static void excutarConsultaD2(EntityManager em) {
		// Estados sem cidade
		// Filtro: (nenhum)
		// Resultado: sigla e nome dos estados que n�o possuem cidades
		// cadastradas
		System.out.println("************************ excutarConsultaD2 ****************************");
		String jpql = "select e from Estado e where e.cidades.size = 0";
		TypedQuery<Estado> query = em.createQuery(jpql, Estado.class);
		List<Estado> estados = query.getResultList();
		System.out.println(estados);
	}

	private static void excutarConsultaE(EntityManager em) {
		// Quantidade vendedores por cidade
		// Filtro: (nenhum)
		// Resultado: nome da cidade e quantidade de vendedores dessa cidade
		System.out.println("************************ excutarConsultaE ****************************");
		String jpql = "select new " + CidadeVendedorConsultaEDto.class.getCanonicalName()
				+ "(v.endereco.cidade.nome, count(*)) from Vendedor v group by v.endereco.cidade.nome";
		TypedQuery<CidadeVendedorConsultaEDto> query = em.createQuery(jpql, CidadeVendedorConsultaEDto.class);
		List<CidadeVendedorConsultaEDto> cidadesVendedores = query.getResultList();
		System.out.println(cidadesVendedores);
	}

	private static void excutarConsultaF(EntityManager em) {
		// Vendedores de uma cidade
		// Filtro: nome da cidade
		// Resultado: nome e telefone do vendedor
		System.out.println("************************ excutarConsultaF ****************************");
		String jpql = "select v.nome, t from Vendedor v inner join v.telefones t where v.endereco.cidade.nome = :nomeCidade";
		Query query = em.createQuery(jpql);
		query.setParameter("nomeCidade", "Salvador");
		List<Object[]> resultados = query.getResultList();
		for (Object[] linha : resultados) {
			System.out.println(linha[0] + " | " + linha[1]);
		}
	}

	private static Logger logger = Logger.getLogger(Consulta.class);
}
