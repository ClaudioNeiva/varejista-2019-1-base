package br.ucsal.bes20191.bd2.exerciciojpa2.exemplo;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.Logger;

import br.ucsal.bes20191.bd2.exerciciojpa2.domain.Cidade;
import br.ucsal.bes20191.bd2.exerciciojpa2.domain.Endereco;
import br.ucsal.bes20191.bd2.exerciciojpa2.domain.Estado;
import br.ucsal.bes20191.bd2.exerciciojpa2.domain.SituacaoVendedorEnum;
import br.ucsal.bes20191.bd2.exerciciojpa2.domain.Vendedor;

public class Exemplo1 {

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		try {
			emf = Persistence.createEntityManagerFactory("varejista");
			EntityManager em = emf.createEntityManager();

			em.getTransaction().begin();
			Vendedor vendedor1 = new Vendedor("123", "Claudio", null, null, null, Arrays.asList("712313"), new Date(),
					new Endereco("Rua X", "Bairro y", new Cidade("SSA", "Salvador", new Estado("BA", "Bahia"))),
					BigDecimal.valueOf(.15), SituacaoVendedorEnum.ATIVO, null);
			em.persist(vendedor1);
			em.getTransaction().commit();

			em.getTransaction().begin();
			vendedor1.setNome("Neiva");
			em.getTransaction().commit();

			em.clear();

			em.getTransaction().begin();
			vendedor1.setSituacao(SituacaoVendedorEnum.SUSPENSO);
			em.getTransaction().commit();

			Vendedor vendedor2 = em.find(Vendedor.class, "123");
			logger.info(vendedor2);

			em.close();
		} catch (Exception e) {
			logger.error("Falha no programa principal:", e);
		} finally {
			if (emf != null) {
				emf.close();
			}
		}
	}

	private static final Logger logger = Logger.getLogger(Exemplo1.class.getCanonicalName());

}
